package br.edu.unoesc.edi.mepoupe.dao;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;

import br.edu.unoesc.edi.mepoupe.model.DadosBoleto;




public class DAOManager {
	/**
	 * Cria tabela se n�o existir ou createDao.
	 * @author aline
	 */
	public static Dao<DadosBoleto, String> dadosboletoDAO;

	
	static {
		try {

			TableUtils.createTableIfNotExists(Connection.getConection(), DadosBoleto.class);
		
			
			dadosboletoDAO = DaoManager.createDao(Connection.getConection(), DadosBoleto.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
