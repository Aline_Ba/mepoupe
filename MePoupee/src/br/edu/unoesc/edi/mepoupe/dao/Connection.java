package br.edu.unoesc.edi.mepoupe.dao;

	import java.sql.SQLException;
	import com.j256.ormlite.jdbc.JdbcConnectionSource;
	import com.j256.ormlite.support.ConnectionSource;

	public class Connection {
		private static final String DB_URL = "jdbc:sqlite:db/mepoupe.db";
		private static ConnectionSource connectionSource = null;
		
		private Connection() {
			
		}
		/**
		 * Cria uma conex��o com a database
		 * @return Conex�o
		 * @author aline
		 */
		public static ConnectionSource getConection(){
			if(connectionSource == null){
				try{
					try{
						Class.forName("org.sqlite.JDBC");
					}catch(ClassNotFoundException e){
						e.printStackTrace();
					}
					connectionSource = new JdbcConnectionSource(DB_URL);
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
			return connectionSource;
		}
	}

