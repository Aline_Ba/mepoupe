package br.edu.unoesc.edi.mepoupe.model;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Cria��o das tabelas e das colunas usadas no projeto
 * @author aline
 * */
@DatabaseTable(tableName = "DadosBoleto")
public class DadosBoleto {
	
	@DatabaseField(generatedId = true)
	private Integer id;
	
	@DatabaseField(canBeNull = false)
	private String fornecedor;
	
	@DatabaseField(canBeNull = false)
	private float valor;
	
	@DatabaseField(canBeNull = false)
	private long vencimento;

	


	/**
	 * GETTERS
	 * @author aline
	 * */
	public Integer getId() {
		return id;
	}
	
	public String getFornecedor() {
		return fornecedor;
	}
	
	public float getValor() {
		return valor;
	}
	
	public long getVencimento() {
		return vencimento;
	}


	
	/**
	 * SETTERS
	 * @author aline
	 * */
	public void setId(Integer id) {
		this.id = id;
	}

	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setValor(float valor) {
		this.valor = valor;
	}

	

	public void setVencimento(long vencimento) {
		this.vencimento = vencimento;
	}

	
	
}