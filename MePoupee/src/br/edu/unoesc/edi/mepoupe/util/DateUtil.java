package br.edu.unoesc.edi.mepoupe.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static SimpleDateFormat sdf;

	static {
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	public static String convertDateToString(Date date) {
		return sdf.format(date);
	}

	public static Date convertStringToDate(String date) {
		Date dt = null;
		try {
			dt = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dt;
	}

}
