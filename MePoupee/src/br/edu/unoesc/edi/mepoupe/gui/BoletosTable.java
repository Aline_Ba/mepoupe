package br.edu.unoesc.edi.mepoupe.gui;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import br.edu.unoesc.edi.mepoupe.dao.DAOManager;
import br.edu.unoesc.edi.mepoupe.model.DadosBoleto;
import br.edu.unoesc.edi.mepoupe.util.DateUtil;

public class BoletosTable extends JFrame {
	/**
	 * Listagem dos boletos cadastrados no banco
	 * @author ana
	 */
	public BoletosTable() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(BoletosTable.class.getResource("/images/icone.png")));
		initComponents();
		DefaultTableModel modelo = (DefaultTableModel) getTableBoletos().getModel();
		getTableBoletos().setRowSorter(new TableRowSorter(modelo));

		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
				DefaultTableModel dtmProdutos = (DefaultTableModel) tableBoletos.getModel();
				dtmProdutos.setRowCount(0);
				// listagem
				List<DadosBoleto> dados = null;
				
				try {
					
					dados = DAOManager.dadosboletoDAO.queryBuilder().orderBy("vencimento", true).query();

				} catch (SQLException e) {
					e.printStackTrace();
				}
				if (dados != null) {

					for (DadosBoleto dadosBoleto : dados) {
						dtmProdutos.addRow(new Object[] { dadosBoleto.getFornecedor(),
								DateUtil.convertDateToString(new Date(dadosBoleto.getVencimento())), dadosBoleto.getValor() });
					}
				}
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				}

			}
		}).start();
	}

	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();

		jPanel2 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();

		setTableBoletos(new javax.swing.JTable());

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGap(0, 499, Short.MAX_VALUE)
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGap(0, 75, Short.MAX_VALUE)
		);
		jPanel1.setLayout(jPanel1Layout);

		getTableBoletos().setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "Fornecedor", "Vencimento", "Valor" }) {
			boolean[] canEdit = new boolean[] { false, false, false };

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		});

		jScrollPane1.setViewportView(getTableBoletos());
		btnCadastrar = new javax.swing.JButton();
		btnCadastrar.setIcon(new ImageIcon(BoletosTable.class.getResource("/images/novo.png")));
		btnCadastrar.setText("Cadastrar");
		btnCadastrar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnCadastrarActionPerformed(evt);
			}
		});

		btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSair.setIcon(new ImageIcon(BoletosTable.class.getResource("/images/sair.png")));

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel2Layout.createSequentialGroup()
							.addGap(11)
							.addComponent(btnCadastrar)
							.addPreferredGap(ComponentPlacement.RELATED, 300, Short.MAX_VALUE)
							.addComponent(btnSair))
						.addGroup(jPanel2Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)))
					.addContainerGap())
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
					.addGap(18)
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSair)
						.addComponent(btnCadastrar))
					.addContainerGap())
		);
		jPanel2.setLayout(jPanel2Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
				.addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, 492, Short.MAX_VALUE)
					.addGap(22))
		);
		getContentPane().setLayout(layout);

		pack();

		
	}

	private void btnCadastrarActionPerformed(java.awt.event.ActionEvent evt) { // AQUI ESTE CU
		Cadastrar cadastro = new Cadastrar();
		cadastro.setVisible(true);

		
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BoletosTable frame = new BoletosTable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public javax.swing.JTable getTableBoletos() {
		return tableBoletos;
	}

	public void setTableBoletos(javax.swing.JTable tableBoletos) {
		this.tableBoletos = tableBoletos;
	}
	private javax.swing.JButton btnCadastrar;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable tableBoletos;
	private JButton btnSair;

}
