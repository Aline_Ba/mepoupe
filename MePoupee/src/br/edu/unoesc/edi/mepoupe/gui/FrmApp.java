package br.edu.unoesc.edi.mepoupe.gui;

import java.awt.Button;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class FrmApp extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 * @author ana
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmApp frame = new FrmApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @author ana
	 */
	public FrmApp() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FrmApp.class.getResource("/images/icone.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 616, 385);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("CheckBox.background"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel PapelParede = new JLabel("");
		PapelParede.setBackground(UIManager.getColor("Button.focus"));
		PapelParede.setIcon(new ImageIcon(FrmApp.class.getResource("/images/lblfundo.png")));
		PapelParede.setBounds(118, 26, 472, 321);
		contentPane.add(PapelParede);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 590, 22);
		contentPane.add(menuBar);
		
		JMenu mnSobre = new JMenu("Arquivo");
		mnSobre.setActionCommand("Arquivo");
		menuBar.add(mnSobre);
		
		JMenuItem mntmSobre = new JMenuItem("Sobre");
		mnSobre.add(mntmSobre);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mnSobre.add(mntmSair);
		mntmSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		Button btnBoleto = new Button("BOLETOS");
		btnBoleto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BoletosTable boleto = new BoletosTable();
				boleto.setVisible(true);
			}
		});
		btnBoleto.setBounds(0, 26, 112, 36);
		contentPane.add(btnBoleto);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
