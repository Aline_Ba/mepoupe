package br.edu.unoesc.edi.mepoupe.gui;

import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Label;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.ComponentOrientation;
import javax.swing.JPasswordField;
import java.awt.event.KeyAdapter;


/** 
 * @since 2018
 * @version 1.0
 *  */
public class FrmInicio extends JFrame {

	private JPanel contentPane;
	private JTextField txtLogin;
	private JTextField txtIdentificaoDeUsurio;
	private JButton btnAcessar;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 * @author ana
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmInicio frame = new FrmInicio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @author ana
	 */
	
	
	public FrmInicio() {
		setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		setIconImage(Toolkit.getDefaultToolkit().getImage(FrmInicio.class.getResource("/images/icone.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtLogin = new JTextField();
		txtLogin.setForeground(Color.GRAY);
		txtLogin.setBounds(147, 133, 137, 22);
		contentPane.add(txtLogin);
		txtLogin.setColumns(10);
		
		JLabel img = new JLabel("");
		img.setIcon(new ImageIcon(FrmInicio.class.getResource("/images/img.png")));
		img.setBounds(181, 22, 89, 66);
		contentPane.add(img);
		
		txtIdentificaoDeUsurio = new JTextField();
		txtIdentificaoDeUsurio.setEditable(false);
		txtIdentificaoDeUsurio.setIgnoreRepaint(true);
		txtIdentificaoDeUsurio.setOpaque(false);
		txtIdentificaoDeUsurio.setRequestFocusEnabled(false);
		txtIdentificaoDeUsurio.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtIdentificaoDeUsurio.setBorder(null);
		txtIdentificaoDeUsurio.setText("Identifica\u00E7\u00E3o de Usu\u00E1rio");
		txtIdentificaoDeUsurio.setBounds(156, 92, 128, 20);
		contentPane.add(txtIdentificaoDeUsurio);
		txtIdentificaoDeUsurio.setColumns(10);
		
		btnAcessar = new JButton("Acessar");
		btnAcessar.setHorizontalAlignment(SwingConstants.LEFT);
		btnAcessar.setVerticalAlignment(SwingConstants.BOTTOM);
		btnAcessar.setIcon(new ImageIcon(FrmInicio.class.getResource("/images/ok.png")));
		
		txtSenha = new JPasswordField();
		
		txtSenha.setBounds(147, 178, 137, 20);
		contentPane.add(txtSenha);
		
		txtSenha.addKeyListener(new KeyAdapter() {
			
			public void keyPressed(KeyEvent evt) {
				if (evt.getKeyCode()== KeyEvent.VK_ENTER) {
					if (txtLogin.getText().equals("usuario") && (txtSenha.getText().equals("1234"))) {
						FrmApp app = new FrmApp ();
			               app.setVisible(true);
					} else {
						JOptionPane.showMessageDialog(null, "Acesso Negado");
						
					}}}
			
		});
		btnAcessar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (txtLogin.getText().equals("usuario") && (txtSenha.getText().equals("1234"))) {
					FrmApp app = new FrmApp ();
		               app.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Acesso Negado");
					

				}	
					
	               
				
			}
		});
		
		btnAcessar.setBounds(157, 209, 112, 29);
		contentPane.add(btnAcessar);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Arial", Font.PLAIN, 10));
		lblSenha.setBounds(147, 161, 46, 14);
		contentPane.add(lblSenha);
		
		JLabel lblUsurio = new JLabel("Usu\u00E1rio");
		lblUsurio.setFont(new Font("Arial", Font.PLAIN, 10));
		lblUsurio.setBounds(147, 110, 46, 22);
		contentPane.add(lblUsurio);
		
		
		
		
	}
}
