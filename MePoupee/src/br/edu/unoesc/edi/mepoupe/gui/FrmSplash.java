package br.edu.unoesc.edi.mepoupe.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;

public class FrmSplash extends JFrame {

	private JPanel contentPane;
	private Timer t;
	private JTextField txtLoading;

	/**
	 * Launch the application.
	 * @author ana
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmSplash frame = new FrmSplash();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @author ana
	 */
	public FrmSplash() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FrmSplash.class.getResource("/images/icone.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Button.light"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Fundo = new JLabel("");
		Fundo.setForeground(Color.LIGHT_GRAY);
		Fundo.setIcon(new ImageIcon(FrmSplash.class.getResource("/images/me poupee.png")));
		Fundo.setBounds(54, 11, 320, 202);
		contentPane.add(Fundo);
		
		JProgressBar Carregando = new JProgressBar();
		Carregando.setBounds(10, 224, 387, 20);
		Carregando.setIndeterminate(true);
		contentPane.add(Carregando);
		
		txtLoading = new JTextField();
		txtLoading.setHorizontalAlignment(SwingConstants.CENTER);
		txtLoading.setFocusable(false);
		txtLoading.setFont(new Font("Tahoma", Font.ITALIC, 11));
		txtLoading.setBorder(null);
		txtLoading.setBackground(UIManager.getColor("Button.light"));
		txtLoading.setForeground(UIManager.getColor("Button.focus"));
		txtLoading.setText("Loading...");
		txtLoading.setBounds(172, 242, 86, 20);
		contentPane.add(txtLoading);
		txtLoading.setColumns(10);
		setLocationRelativeTo(null);
		
		t = new Timer(4000, new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// abre o formulário do jogo
				FrmInicio frmInicio = new FrmInicio();
				frmInicio.setVisible(true);		
				// interrope o timer
				t.stop();
				// fechando splash
				dispose();
			}
			
		});
		t.start();
		
	}
	}

