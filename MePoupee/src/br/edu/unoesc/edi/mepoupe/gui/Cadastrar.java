package br.edu.unoesc.edi.mepoupe.gui;

import java.awt.EventQueue;
//import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.j256.ormlite.support.ConnectionSource;

import br.edu.unoesc.edi.mepoupe.dao.Connection;
import br.edu.unoesc.edi.mepoupe.dao.DAOManager;
import br.edu.unoesc.edi.mepoupe.model.DadosBoleto;
import br.edu.unoesc.edi.mepoupe.util.DateUtil;
import java.awt.Toolkit;

public class Cadastrar extends JFrame {

	private JPanel contentPane;
	private JTextField txtFornecedor;
	private JTextField txtVencimento;
	private JTextField txtValor;
	private JButton btnConfirmar;
	private JButton btnCancelar;
	private JLabel lblValor;

	/**
	 * Launch the application.
	 * @author ana
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cadastrar frame = new Cadastrar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * @author ana
	 * Quando clica em cadastrar no BoletosTable
	 * */
	

	public Cadastrar() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Cadastrar.class.getResource("/images/icone.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 357, 334);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtFornecedor = new JTextField();
		txtFornecedor.setBounds(20, 42, 311, 20);
		contentPane.add(txtFornecedor);
		txtFornecedor.setColumns(10);
		
		txtVencimento = new JTextField();
		txtVencimento.setBounds(20, 104, 120, 20);
		contentPane.add(txtVencimento);
		txtVencimento.setColumns(10);
		
		txtValor = new JTextField();
		txtValor.setBounds(20, 174, 86, 20);
		contentPane.add(txtValor);
		txtValor.setColumns(10);
		
		/**
		 * @author aline
		 * Passa para o banco de dados as informações
		 * dos campos txt
		 * */
		
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {				
					
					DadosBoleto db = new DadosBoleto();
					db.setFornecedor(txtFornecedor.getText());
					db.setValor(Float.parseFloat(txtValor.getText()));
					Date dt = DateUtil.convertStringToDate(txtVencimento.getText());
					 // para gravar no banco como um long
					db.setVencimento(dt.getTime());
					DAOManager.dadosboletoDAO.create(db);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			
		});
		btnConfirmar.setIcon(new ImageIcon(Cadastrar.class.getResource("/images/ok.png")));
		btnConfirmar.setBounds(17, 256, 130, 29);
		contentPane.add(btnConfirmar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setIcon(new ImageIcon(Cadastrar.class.getResource("/images/sair.png")));
		btnCancelar.setBounds(185, 256, 127, 29);
		contentPane.add(btnCancelar);
		
		JLabel lblFornecedor = new JLabel("* Fornecedor");
		lblFornecedor.setFont(new Font("Arial", Font.PLAIN, 12));
		lblFornecedor.setBounds(20, 17, 86, 14);
		contentPane.add(lblFornecedor);
		
		JLabel lblVencimento = new JLabel("* Data de Vencimento");
		lblVencimento.setFont(new Font("Arial", Font.PLAIN, 12));
		lblVencimento.setBounds(20, 73, 127, 26);
		contentPane.add(lblVencimento);
		
		lblValor = new JLabel("* Valor");
		lblValor.setFont(new Font("Arial", Font.PLAIN, 12));
		lblValor.setBounds(20, 149, 63, 14);
		contentPane.add(lblValor);
	
	
	
	
		
		
		
	}
	
	
	

}
